var express = require('express')
var app = express()
var bodyParser = require('body-parser')
app.use(bodyParser.json())


const webPush = require('web-push')

const port = "8080";

// Falls die Environment-Variablen nicht gesetzt sind, werden welche generiert und ausgegeben.
if (!process.env.VAPID_PUBLIC_KEY || !process.env.VAPID_PRIVATE_KEY) {
  console.log("VAPID: ",webPush.generateVAPIDKeys())
  return;
}

if(!process.env.VAPID_EMAIL){
  console.log("VAPID_MAIL is not set!");
  return;
}

if(!process.env.BASE_URL){
  console.log("BASE_URL is not set!");
  return;
}

// zur Identifikation muss eine Emailadresse hinterlegt werden.
webPush.setVapidDetails(
  'mailto:'+process.env.VAPID_EMAIL,
  process.env.VAPID_PUBLIC_KEY,
  process.env.VAPID_PRIVATE_KEY
);

function sendNotification(subscription,payload) {
  webPush.sendNotification(subscription,JSON.stringify(payload)).then(function() {
    console.log('Push Application Server - Notification sent to ' + subscription.endpoint);
  }).catch(function(err) {
    console.log('ERROR in sending Notification, endpoint removed ' + subscription.endpoint);
    delete subscriptions[subscription.endpoint];
  });
}

app.use( express.static('public'));

app.get('/', function (req, res) {
  res.redirect('index.html');
})

app.get('/push/vapidPublicKey', function(req, res) {
  res.send(process.env.VAPID_PUBLIC_KEY);
});

var subscriptions = {};
var answers = [];

app.post('/push/register', function(req, res) {
  var subscription = req.body.subscription;
  if (!subscriptions[subscription.endpoint]) {
    console.log('Subscription registered ' + subscription.endpoint);
    subscriptions[subscription.endpoint] = subscription;
  }
  res.sendStatus(201);
});

app.post('/push/unregister', function(req, res) {
  var subscription = req.body.subscription;
  if (subscriptions[subscription.endpoint]) {
    console.log('Subscription unregistered ' + subscription.endpoint);
    delete subscriptions[subscription.endpoint];
  }
  res.sendStatus(201);
});

app.post('/push/isregister', function(req, res) {
  var subscription = req.body.subscription;
  if (subscriptions[subscription.endpoint]) {
    res.send(true);
  }else{
    res.send(false);
  }

});

app.post('/push/notify', function(req, res) {
  var name = req.body.name;
  let payload = {
    title: "Title",
    options: {
      body: "Push"
    }
  }
  Object.values(subscriptions).forEach(function(subscription){
    sendNotification(subscription,payload);
  });
  res.sendStatus(201);
});

app.post('/push/ask', function(req, res) {
  var question = req.body.question;
  let payload = {
    title: "Wichtige Frage:",
    options: {
      body: question,
      actions: [
        { action: process.env.BASE_URL+"/push/action/yes", title: "Ja" },
        { action: process.env.BASE_URL+"/push/action/no", title: "Nein" }
      ]
    }
  }
  Object.values(subscriptions).forEach(function(subscription){
    sendNotification(subscription,payload);
  });
  answers.push({question:question,yes:0,no:0});
  res.sendStatus(201);
});

app.get('/push/action/:action', function(req, res) {
  console.log(req.params.action);
  answers[answers.length-1][req.params.action]++;
  res.redirect('/');
});

app.get('/push/answers', function(req, res) {
  res.send(JSON.stringify(answers));
});

app.listen(port, function() {
  console.log('Push app is listening on port '+port+'.')
})
