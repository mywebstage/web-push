if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('service-worker.js').then(function(reg) {
    console.log('Service-Worker registered. Scope is ' + reg.scope);
  }).catch(function(error) {
    console.log('Error at reqiser Service-Worker: ' + error);
  });

  navigator.serviceWorker.ready.then(function(registration) {
    return registration.pushManager.getSubscription();
  }).then(function(subscription) {
    if (subscription) {
      console.log('Already subscribed', subscription.endpoint);
      subscribe();
    } else {
      console.log('Not subscribed');
    }
  });

}else{
  console.error("Browser does not Support Service Worker!");
}

function subscribe() {
  navigator.serviceWorker.ready.then(async function(registration) {

    // Vapid Key vom Server abfragen
    const response = await fetch('/push/vapidPublicKey');
    const vapidPublicKey = await response.text();
    const convertedVapidKey = urlBase64ToUint8Array(vapidPublicKey);

    // Subscriben
    return registration.pushManager.subscribe({
      userVisibleOnly: true,
      applicationServerKey: convertedVapidKey
    });
  }).then(function(subscription) {
    console.log('Subscribed', JSON.stringify(subscription));
    $("#subscribe").addClass("disabled");
    $("#unsubscribe").removeClass("disabled");

    // Subscription an Server senden
    return fetch('/push/register', {
      method: 'post',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        subscription: subscription
      })
    });
  });
}
function unsubscribe() {
  navigator.serviceWorker.ready.then(function(registration) {
    return registration.pushManager.getSubscription();
  }).then(function(subscription) {

    // abmelden
    return subscription.unsubscribe().then(function() {
        console.log('Unsubscribed', JSON.stringify(subscription));
        $("#unsubscribe").addClass("disabled");
        $("#subscribe").removeClass("disabled");

        // dem Server mitteilen, dass abgemeldet wurde
        return fetch('/push/unregister', {
          method: 'post',
          headers: {
            'Content-type': 'application/json'
          },
          body: JSON.stringify({
            subscription: subscription
          })
        });
      });
  });
}

function ask(){
  let question = $('#question').val();

  // Frage an den Server senden, damit diese diese verteilt
  return fetch('/push/ask', {
    method: 'post',
    headers: {
      'Content-type': 'application/json'
    },
    body: JSON.stringify({
      question: question
    })
  });
}

var fetch_answers = function(){

  // Antworten im 3 Sekundenintervall abfragen und als Tabelle darstellen

  $.getJSON('/push/answers',function(response){

    if(response){
      $("#auswertung").html("");
      let table = "";
      response.reverse();
      response.forEach(function(question){
        table += "<table style='margin-bottom:2rem'>";
        table += "<tr>";
        table += "<td colspan=2>"+question.question+"</td>";
        table += "</tr>";
        table += "<tr>";
        table += "<td>Ja</td>";
        table += "<td>"+question.yes+"</td>";
        table += "</tr>";
        table += "<tr>";
        table += "<td>Nein</td>";
        table += "<td>"+question.no+"</td>";
        table += "</tr>";
        table += "</table>";
      })
      $("#auswertung").append(table);
    }
    setTimeout(fetch_answers,3000);
  });
}
fetch_answers();




function urlBase64ToUint8Array(base64String) {
  var padding = '='.repeat((4 - base64String.length % 4) % 4);
  var base64 = (base64String + padding)
    .replace(/\-/g, '+')
    .replace(/_/g, '/');

  var rawData = window.atob(base64);
  var outputArray = new Uint8Array(rawData.length);

  for (var i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i);
  }
  return outputArray;
}
