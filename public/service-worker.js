self.addEventListener('push', function(event) {
  let payload = event.data ? event.data.text() : '[]';
  let payloadData = JSON.parse(payload);
  event.waitUntil(self.registration.showNotification(payloadData.title, payloadData.options));
});

self.addEventListener('notificationclick', function(event) {
  event.notification.close();
  if(event.action){
    event.waitUntil(
      clients.openWindow(event.action)
    );
  }
});
